[ ![Codeship Status for tylerhoran/asha](https://codeship.com/projects/8facdc20-4c39-0132-44ef-36f51938a765/status?branch=master)](https://codeship.com/projects/47015)
[![Code Climate](https://codeclimate.com/repos/551743e96956801d48001d85/badges/f9a1213e5e7e08900bb5/gpa.svg)](https://codeclimate.com/repos/551743e96956801d48001d85/feed)[![Test Coverage](https://codeclimate.com/repos/551743e96956801d48001d85/badges/f9a1213e5e7e08900bb5/coverage.svg)](https://codeclimate.com/repos/551743e96956801d48001d85/feed)

# Asha #
Virtual SMS assistants on demand.

## Introduction ##
This site consists of the following core elements:

1. Rails 4.2.1
2. Ruby 2.2.1
3. Heroku Staging Deployment at https://asha-staging.herokuapp.com
4. Heroku Production Deployment at
   https://asha.io
5. Postgresql Database
7. Memcache via MemCachier
8. Performance monitoring via New Relic
9. Analaytics on Mixpanel (SegmentIO)
10. Email Delivery via Mandrill
11. Error Tracking via Rollbar
12. Static File Hosting Via Amazon S3
13. Content Delivery Network via Amazon Cloudfront
14. Continuous Integration via Codeship


## Development Environment ##
To set up the development environment on OSX:

1. Install RVM and bundle install

    ```
    \curl -sSL https://get.rvm.io | bash
    ```

2. Install postgresql

    ```
    brew install postgresql
    ```

    Adding the script to launch postres on login

    ```
    mkdir -p ~/Library/LaunchAgents
    ```

    ```
    cp /usr/local/Cellar/postgresql/9.3.2/homebrew.mxcl.postgresql.plist ~/Library/LaunchAgents/
    ```

    ```
    launchctl load -w ~/Library/LaunchAgents/homebrew.mxcl.postgresql.plist
    ```

3. Install s3cmd

    ```
    brew install s3cmd
    ```

4. Run the rake database create and seed scripts

    ```
    rake db:create
    rake db:migrate
    rake db:seed
    ```

### Development Elements ###

-   Template Engine: Haml
-   Testing Framework: RSpec and Factory Girl
-   Form Builder: SimpleForm
-   Authentication: Devise
-   Authorization: CanCan
-   Server: Pow
-   File management: s3cmd

## Testing Environment ##

The test suite consists of the following elements:

1. Rspec
2. Capybara
3. Factory Girl
4. Webkit
5. Guard
6. Spring

To run the full test suite from project root:

  ```
  guard; all
  ```
## Deploying the application ##

To deploy to all applications (with migrations and tests):

  ```
  git push origin master
  ```

## Managing Data ##

To copy production data and assets to the staging environment:

  ```
  rake copy:production_to_staging
  ```

To copy staging data and assets to the production environment:

  ```
  rake copy:staging_to_production
  ```

To copy development data to the staging environment:

  ```
  rake copy:development_to_staging
  ```

To copy production data to the development environment:

  ```
  rake copy:production_to_development
  ```


Contributing
------------

If you make improvements to this application, please first check out a new branch.

-   Make your feature addition or bug fix.
-   Commit with Git.
-   Notify others on the development team before merging to the main branch.

If you add functionality to this application, create an alternative
implementation, or build an application that is similar, please contact
me and I’ll add a note to the README so that others can find your work.

Credits
-------

For information about the source please contact:

Tyler J. Horan, PhD

License
-------

Copyright &copy; 2014 Horan LLC.
All rights reserved.

Redistribution and use in source and binary forms is not permitted and other materials related to such distribution may not be distributed. The name of Asha may not be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.


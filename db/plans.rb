Plan.create([{ name: 'Basic', price: 99.00, interval: 'month', stripe_id: '1',
               features: ['Available 9 - 5', 'Responds Instantly', 'Unlimited Tasks', 'Tasks completed in under 8 hours'].join("\n\n"),
               display_order: 1 },
             { name: 'Standard', price: 199.00, interval: 'month', stripe_id: '2',
               features: ['Available 9 - 5', 'Responds Instantly', 'Unlimited Tasks', 'Tasks completed in under 4 hours'].join("\n\n"),
               display_order: 2 },
             { name: 'Business', price: 499.00, interval: 'month', stripe_id: '3',
               features: ['Available 9 - 5', 'Responds Instantly', 'Unlimited Tasks', 'Tasks completed in under 1 hour'].join("\n\n"),
               display_order: 3 }])

class CreateCharges < ActiveRecord::Migration
  def change
    create_table :charges do |t|
      t.references :user, index: true
      t.string :token
      t.float :amount
      t.string :description

      t.timestamps
    end
  end
end

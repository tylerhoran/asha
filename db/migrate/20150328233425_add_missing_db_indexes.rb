class AddMissingDbIndexes < ActiveRecord::Migration
  def change
    add_index :accounts, :user_id
    add_index :accounts, :client_id
    add_index :subscriptions, :plan_id
    add_index :subscriptions, :coupon_id
    add_index :subscriptions, :user_id
    add_index :users, :invited_by_type
  end
end

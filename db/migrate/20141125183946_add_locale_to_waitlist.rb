class AddLocaleToWaitlist < ActiveRecord::Migration
  def change
    add_column :waitlists, :locale, :string, default: 'en'
  end
end

class AddTwilioFieldsToUser < ActiveRecord::Migration
  def change
    add_column :users, :twilio_number, :string
    add_column :users, :twilio_pathway, :string
  end
end

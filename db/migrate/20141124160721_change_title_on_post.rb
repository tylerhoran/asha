class ChangeTitleOnPost < ActiveRecord::Migration
  def change
    remove_column :posts, :title, :string
    add_column :posts, :name, :string
  end
end

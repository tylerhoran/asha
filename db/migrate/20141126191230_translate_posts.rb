class TranslatePosts < ActiveRecord::Migration
  def self.up
    say_with_time('Creating translation table for posts') do
      Post.create_translation_table!(name: :string, body: :text, migrate_data: true)
    end
  end

  def self.down
    say_with_time('Removing translation table for posts') do
      Post.drop_translation_table! migrate_data: true
    end
  end
end

Post.create(name: "Aren't there apps for that already?",
            body: "I hear a number of people asking that question each \
                  time they hear that someone hired a virtual assistant. \
                  While they are certainly correct that there are apps for \
                  nearly everything that a virtual assistant can do, they're \
                  often missing the bigger point: they're the ones that have \
                  to use them. It's time consuming not in their use, but in \
                  the mental taxation required in organizing one's life.\
                  <br /><br />As any good small business owner knows, you have \
                  to delegate. The more that you focus on the small things, the \
                  less time you have to spend on the big picture and strategy of \
                  your business. Sure, you could pull out Kayak and spend an hour \
                  looking through all the best options for traveling to another \
                  country and booking hotel arrangements, but that's an hour that \
                  you can't focus on more pressing tasks in building your business. \
                  And it's an even greater time-suck worrying about the logistics of \
                  various contingencies. Where am I going to eat? Do I need to plan that \
                  in advance? What if the client wants to go out to dinner? How much time \
                  do I really need to devote to this trip? <br /><br />One of the \
                  most important lessons that you can learn from building a \
                  business from the ground floor is that you don't have time to do \
                  everything yourself. Lets take another example of market research. \
                  One of the most precious commodities for any busy person is time. It's \
                  hard to schedule, it's even harder to organize the amount of time that \
                  should be spent on various tasks and duties. <br /><br />I remember the \
                  first time that I heard of someone paying to have someone schedule and \
                  coordinate the small tasks in their life. I thought they were just plain \
                  lazy, or they weren't properly using the tech tools that were available. \
                  10 Years into a small business I learned that they were just thinking \
                  like an employee rather than an employer. Your time is an investment and \
                  you want the greatest return on that investment. Organization, scheduling, \
                  researching have some of the smallest returns on investment for time spent. \
                  <br /><br />It's time to start investing your time better. That's why I \
                  created Asha.")
Post.create(name: 'Building a customer contact & leads list',
            body: "At some point in the lifecycle of a business there becomes the need to \
                  develop a customer base that will eventually purchase the product or service \
                  that's on offer. While the business owner can spend time and effort collecting \
                  emails, phone number or other contact information, it's much easier – and likely \
                  more productive – to outsource this task to a large number of people or to an \
                  assistant.<br /><br />Consider that you are building up a small roof repair \
                  business. You know the whole population of the local city to be about 20,000 \
                  households. Consider that you are interested in determining the percentage of \
                  those households that were build over 30 years ago. While you could certainly \
                  look at the titles for all of the houses in the city records, why not have an \
                  assistant generate all the reports that you need. This frees up your time to \
                  strategize how you will reach out to those people. <br /><br />Will you contact \
                  them via direct-mail? Or will you gather their phone numbers via the local \
                  yellow-book? You could also have your assistant gather that information as well.\
                  <br /><br />Now that you've determined you potential customer base, you can use an \
                  assistant to deliver all the mail? Need to send out postcards to all those \
                  households? Have the assistant draft up the copy for that mailing and purchase \
                  the postcard delivery from Vistaprint with postage. <br /><br />Your marketing \
                  push? Done.")

Post.create(name: 'Answering customer service emails on asha',
            body: "How often when you're developing a new product or developing a new strategy \
                  are you help back by answering customer support requests or customer service issues? \
                  More often than not, the requests are simple things like forgotten passwords, or \
                  requests for copies of receipts. These requests, while easy enough to handle on a \
                  case-by-case basis are not necessarily enough to hire a full-time employee. If \
                  you're only spending 5 hours a week on it, is it really worth it to hire a \
                  full-time or even part-time employee? Probably not.<br /><br />Is there a better \
                  way of handling these issues? Sure. You can hire an outsourced customer service \
                  firm, but you may not have the volume of requests to justify the expenditure. Or \
                  you could just roll these tasks into the responsibilities of your virtual assistant. \
                  <br /><br />They may already be working on your marketing tasks, or they may be \
                  helping you find the best travel arrangements to fit your busy schedule. There's \
                  no need to commit the hours in advance that you're willing to pay this assistant. \
                  If you're a subscriber to Asha, all you need to do is determine how fast you need \
                  these tasks completed. Do they need to be completed today, or in 4 hours, or ASAP. \
                  <br /><br />You set the expediency on Asha. You determine the volume of work that \
                  you need accomplished. ")

Post.create(name: 'Dropbox & google drive organization',
            body: "Cloud storage has become ubiquitous these days. From the basic backup services \
                  of Amazon to the more complex asynchronous file backup services of DropBox and Google \
                  Drive, it's easier than every to duplicate your files and save them from loss. \
                  <br /><br />This also means that it's even easier to organize your life directly \
                  from the cloud. No need to have someone login to your computer to organize the \
                  things in your life. Simply give someone access to your Google drive or DropBox \
                  and they can get you set up and organized from the comforts of their own home. \
                  <br /><br />We see this as one of the most common tasks for assistants on Asha. \
                  Need to spend time organizing a folder full of receipts. Simply give your assistant \
                  access to the files and they can get you all sorted.")

Post.create(name: 'Survey and Form Creation',
            body: "Need to gather information from your customers? Looking for feedback on \
                  the latest product that you released? Have your assistant develop the questionnaire \
                  and analyze the results. Perhaps you're interested in knowing how your customers feel \
                  about the change in the search page on your ecommerce site? You could A/B test the \
                  amount of traffic that goes through the page, but you might also get valuable \
                  feedback from your customers themselves. <br /><br />Have your assistant create a \
                  Google form and send invites to your customers. You can gather information about \
                  their responses and have the results sent directly to your email. You can also \
                  generate actionable results from the insights gathered from your assistant. Simplify \
                  the data gathering in your business and make the decision process easier.")

Post.create(name: 'Virtual Assistance Needed',
            body: "How you found yourself in the position of trying to do more than you can \
                  handle? Or have you not yet reached the level of profitability in your business \
                  where you find virtual assistance needed? It would be easy to hire a virtual \
                  assistant today, but would you still need them tomorrow?<br /><br />At Asha, \
                  we alleviate these concerns by making virtual assistance on-demand rather than \
                  on a monthly or even hourly basis. There are no contracts, no employee agreements \
                  to sign. All you need is a low monthly subscription and you can request all the \
                  tasks that you need for the day. If you don't have anything to request tomorrow \
                  don't worry. You won't be changed for anything outside of your subscription. We \
                  make virtual assistance easy.")

Post.create(name: 'Virtual assistant services',
            body: 'What would you do if there were 5 versions of yourself? How much could you \
                  get accomplished during the day? If you were able to delegate many different tasks \
                  so that you could focus on the big picture, would you do it? Or would you delegate \
                  the big picture to another one of yourself?<br /><br />There are hundreds of ways \
                  in which the daily tasks that you have in running a business can become burdensome. \
                  Using various virtual assistant services can get you part of the way to success, \
                  however, often the needs of a virtual assistant are not known at the start of every \
                  day. How much time are you going to spend on having an assistant? Will you have \
                  enough work to justify the need for virtual assistance? <br /><br />At Asha, we \
                  make the decision easy. Rather than paying for the hours that you may need for the \
                  virtual assistant in advance or even after the fact, we allow you to request as \
                  much or as little for a low monthly subscription fee. No more worrying about whether \
                  you have enough money or time to justify the expense. Simply subscribe and your done. \
                  Request a task whenever you need it via text message.')

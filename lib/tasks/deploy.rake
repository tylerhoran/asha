namespace :deploy do
  def heroku(command)
    system("GEM_HOME='' BUNDLE_GEMFILE='' GEM_PATH='' RUBYOPT='' /usr/local/heroku/bin/heroku #{command}")
  end

  [:production].each do |target|
    desc "Deploy the app to #{target}"
    task target => "deploy:#{target}:list_undeployed" do
      Bundler.with_clean_env do
        system "git push -f #{@remote} #{@ref}:master"
        system "heroku run rake db:migrate --app=#{@app}"
        system %{heroku config:add ASHA_REVISION=$(git log -1 --pretty=format:"%h" #{@ref}) --app=#{@app}}
      end
    end

    namespace target do
      desc "List what's waiting to be deployed to #{target}"
      task list_undeployed: :vars do
        puts '------------------------------'
        puts "   Deploy to #{target}:"
        puts '------------------------------'
      end

      task :vars do
        case target
        when :production
          @app = 'asha-production'
          @remote = 'production'
          @ref = 'HEAD'
        else
          abort "unexpected target '#{target}'"
        end
      end
    end
  end

  [:github].each do |target|
    desc "Deploy the source to #{target}"
    task target => "deploy:#{target}:list_undeployed" do
      system "git push origin #{@branch}"
    end

    namespace target do
      desc 'what to deploy'
      task list_undeployed: :vars do
        puts '------------------------------'
        puts "    Deploy to #{target}:"
        puts '------------------------------'
      end

      task :vars do
        case target
        when :github
          @app = 'github'
          @branch = `git symbolic-ref --short -q HEAD`
          @ref = 'HEAD'
        else
          about "unexpected target '#{target}'"
        end
      end
    end
  end

  [:staging].each do |target|
    desc "Deploy the app to #{target}"
    task target => "deploy:#{target}:list_undeployed" do
      Bundler.with_clean_env do
        system "heroku maintenance:on --app=#{@app}"
        system "git push -f #{@remote} #{@ref}:master"
        system "heroku run rake db:migrate --app=#{@app}"
        system %{heroku config:add ASHA_REVISION=$(git log -1 --pretty=format:"%h" #{@ref}) --app=#{@app}}
        system "heroku maintenance:off --app=#{@app}"
      end
    end

    namespace target do
      desc "List what's waiting to be deployed to #{target}"
      task list_undeployed: :vars do
        puts '------------------------------'
        puts "   Deploy to #{target}:"
        puts '------------------------------'
      end

      task :vars do
        case target
        when :staging
          @app = 'asha-staging'
          @remote = 'staging'
          @ref = 'HEAD'
        else
          abort "unexpected target '#{target}'"
        end
      end
    end
  end

  [:all].each do |target|
    desc 'Deploy the app to staging, production & github'
    task target => "deploy:#{target}:list_undeployed" do
      Bundler.with_clean_env do
        system "heroku maintenance:on --app=#{@app}"
        system "git push -f #{@remote} #{@ref}:master"
        system "heroku run rake db:migrate --app=#{@app}"
        system %{heroku config:add ASHA_REVISION=$(git log -1 --pretty=format:"%h" #{@ref}) --app=#{@app}}
        system "heroku maintenance:off --app=#{@app}"
        system "git push -f #{@remote2} #{@ref}:master"
        system "heroku run rake db:migrate --app=#{@app2}"
        system %{heroku config:add ASHA_REVISION=$(git log -1 --pretty=format:"%h" #{@ref}) --app=#{@app2}}
        system 'git push origin master'
      end
    end

    namespace target do
      desc "List what's waiting to be deployed to #{target}"
      task list_undeployed: :vars do
        puts '--------------------------------------------'
        puts '   Deploy to staging, production & github'
        puts '--------------------------------------------'
      end

      task :vars do
        case target
        when :all
          @app = 'asha-staging'
          @app2 = 'asha-production'
          @remote = 'staging'
          @remote2 = 'production'
          @ref = 'HEAD'
        else
          abort "unexpected target '#{target}'"
        end
      end
    end
  end
end

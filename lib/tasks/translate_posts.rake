namespace :translate do
  task posts: :environment do
    Post.all.each do |post|
      english_name = post.name
      english_body = post.body
      [:ar, :bn, :de, :en, :es, :fr, :id, :it, :ja, :ko, :nl, :pt, :ru, :sv, :th, :tr, :zh].each do |locale|
        I18n.locale = locale
        post.update_attributes(name: EasyTranslate.translate(english_name, to: locale), body: EasyTranslate.translate(english_body, to: locale))
      end
    end
  end
end

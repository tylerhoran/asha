guard :bundler do
  watch('Gemfile')
end

group :red_green_refactor, halt_on_fail: true do
  guard :rspec, cmd: 'spring rspec --format progress' do
    watch(/^spec\/.+_spec\.rb$/)
    watch(/^lib\/(.+)\.rb$/)     { |m| "spec/lib/#{m[1]}_spec.rb" }
    watch('spec/spec_helper.rb')  { 'spec' }
    watch(/^app\/(.+)\.rb$/) { |m| "spec/#{m[1]}_spec.rb" }
    watch(/^app\/(.*)(\.erb|\.haml|\.slim)$/) { |m| "spec/#{m[1]}#{m[2]}_spec.rb" }
    watch(/^app\/controllers\/(.+)_(controller)\.rb$/)  {
      |m| [ "spec/routing/#{m[1]}_routing_spec.rb",
            "spec/#{m[2]}s/#{m[1]}_#{m[2]}_spec.rb",
            "spec/acceptance/#{m[1]}_spec.rb" ]
    }
    watch(/^spec\/support\/(.+)\.rb$/) { 'spec' }
    watch('config/routes.rb') { 'spec/routing' }
    watch('app/controllers/application_controller.rb') { 'spec/controllers' }
    watch('spec/rails_helper.rb') { 'spec' }
    watch(/^app\/views\/(.+)\/.*\.(erb|haml|slim)/) { |m| "spec/features/#{m[1]}_spec.rb" }
    watch(/^spec\/acceptance\/(.+)\.feature$/)
    watch(/^spec\/acceptance\/steps\/(.+)_steps\.rb/) { |m|
      Dir[File.join("**/#{m[1]}.feature")][0] || 'spec/acceptance'
    }
  end

  guard :rubocop, all_on_start: false, cli: ['--config', 'config/rubocop.yml']  do
    watch(/.+\.rb$/)
    watch(/config\/rubocop\.yml$/) { |m| File.dirname(m[0]) }
  end

  guard :rails_best_practices, run_at_start: false do
    watch(%r{^app/(.+)\.rb$})
  end

end

guard 'livereload' do
  watch(/app\/views\/.+\.(erb|haml|slim)$/)
  watch(/app\/helpers\/.+\.rb/)
  watch(/public\/.+\.(css|js|html)/)
  watch(/config\/locales\/.+\.yml/)
  # Rails Assets Pipeline
  watch(/(app|vendor)(\/assets\/\w+\/(.+\.(css|js|html|png|jpg))).*/) {
    |m| "/assets/#{m[3]}"
  }
end

guard 'pow' do
  watch('.powrc')
  watch('.powenv')
  watch('.rvmrc')
  watch('.ruby-version')
  watch('Gemfile')
  watch('Gemfile.lock')
  watch('config/application.rb')
  watch('config/environment.rb')
  watch(/^config\/environments\/.*\.rb$/)
  watch(/^config\/initializers\/.*\.rb$/)
end

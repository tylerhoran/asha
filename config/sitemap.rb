Sitemap::Generator.instance.load host: 'www.asha.io' do
  path :root, priority: 1
  path :privacy, priority: 0.8, change_frequency: 'weekly'
  path :features, priority: 0.8, change_frequency: 'weekly'
  path :terms, priority: 0.8, change_frequency: 'weekly'
  path :contact, priority: 0.8, change_frequency: 'weekly'
  path :pricing, priority: 0.8, change_frequency: 'weekly'
  path :new_user_session, priority: 0.8, change_frequency: 'weekly'
  path :new_user_password, priority: 0.8, change_frequency: 'weekly'
  resources :posts, objects: proc { Post.all }, change_frequency: 'weekly'
  path :new_waitlist, priority: 0.8, change_frequency: 'weekly'
end

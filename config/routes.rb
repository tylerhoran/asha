Rails.application.routes.draw do
  resources :posts, only: [:show, :index]
  resources :contacts, only: [:create, :new]
  resources :waitlists, only: [:new, :create]

  post 'voice_response', to: 'messages#voice_response'

  authenticate :user, ->(u) { u.admin? } do
    mount Upmin::Engine => '/admin'
  end

  root to: 'visitors#index'
  get :charges, to: 'charges#index'
  get :privacy, to: 'visitors#privacy'
  get :terms, to: 'visitors#terms'
  get :features, to: 'visitors#features'
  get :contact, to: 'contacts#new'
  get :ping, to: 'visitors#ping'
  devise_for :users, controllers: { invitations: 'users/invitations', registrations: 'users/registrations' }
  resources :users, only: [] do
    member do
      post :show
    end
  end

  mount Koudoku::Engine, at: 'base'

  scope module: 'koudoku' do
    get 'pricing' => 'subscriptions#index', as: 'pricing'
    post 'webhooks' => 'webhooks#create'
  end
end

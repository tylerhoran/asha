Analytics = Segment::Analytics.new(
  write_key: ENV['SEGMENT_IO_KEY'],
  on_error: proc { |msg| print msg }
)

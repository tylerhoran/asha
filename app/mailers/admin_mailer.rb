# The mailer for notifying site admin of new user activity
class AdminMailer < ActionMailer::Base
  default from: ENV['ADMIN_EMAIL']

  def over_limit
    mail(to: ENV['ADMIN_EMAIL'],
         subject: 'Over limit for agents',
         content_type: 'multipart/alternative',
         part: { content_type: 'text/html' })
  end

  def new_contact_message(message)
    @message = message
    mail(to: ENV['ADMIN_EMAIL'],
         subject: 'New contact form message',
         content_type: 'multipart/alternative',
         part: { content_type: 'text/html' })
  end

  def new_waitlist(waitlist)
    @waitlist = waitlist
    mail(to: ENV['ADMIN_EMAIL'],
         subject: 'New subscriber to the waitlist.',
         content_type: 'multipart/alternative',
         part: { content_type: 'text/html' })
  end
end

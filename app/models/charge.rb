# Storing the user's charges to their credit card
class Charge < ActiveRecord::Base
  belongs_to :user
  validates_presence_of :user, :token, :amount, :description
end

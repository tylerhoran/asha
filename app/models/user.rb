# The main object for a customer and agent
class User < ActiveRecord::Base
  has_one :subscription
  has_many :charges
  has_many :accounts
  has_many :clients, through: :accounts
  has_one :account_managers, class_name: 'Account', foreign_key: 'client_id'
  has_one :manager, through: :account_managers, source: :user
  after_create :notify_analytics

  enum role: [:user, :agent, :admin] unless instance_methods.include? :role
  after_initialize :set_default_role, if: :new_record?
  before_save :change_phone_number_format

  def notify_analytics
    Analytics.track(user_id: id, event: 'Invite Sent', properties: { user: name })
  end

  def manager_name
    manager ? manager.name : nil
  end

  def set_default_role
    self.role ||= :user
  end

  devise :invitable, :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable

  def change_phone_number_format
    return unless phone_number
    number = GlobalPhone.parse(phone_number)
    self.phone_number = number.international_string
    self.territory = number.territory.name
  end

  def set_test_numbers
    update_attributes(twilio_number: '+15005550006')
    update_attributes(twilio_pathway: 'https://' + ENV['DOMAIN_NAME'] + '/users/' + id.to_s)
  end

  # :nocov:
  def self.create_twilio_client
    Twilio::REST::Client.new(ENV['TWILIO_ACCOUNT_SID'], ENV['TWILIO_AUTH_TOKEN'])
  end
  # :nocov:

  def self.send_message(to, from, message)
    User.create_twilio_client.account.messages.create(body: message, to: to, from: from) unless Rails.env.test?
  end

  def translate_message(body, m_locale)
    EasyTranslate.translate(body, to: m_locale)
  end

  def send_message_to_agent(agent, sender, body)
    message_body = translate_message(body, agent.locale)
    Message.create(recipient: agent.id, sender: sender.id, message: message_body)
    User.send_message(agent.phone_number, sender.twilio_number, message_body)
    Analytics.track(user_id: id, event: 'User Message', properties: { user: name, recipient: agent.name })
  end

  def send_not_assigned_message(sender)
    message_body = translate_message('You have not been assigned an agent yet.', sender.locale)
    User.send_message(sender.phone_number, ENV['TWILIO_NUMBER'], message_body)
    Message.create(recipient: sender.id, sender: User.admin.first.id, message: message_body)
    Analytics.track(user_id: sender.id, event: 'User Message Without Agent', properties: { user: sender.name })
  end

  def send_message_if_has_agent(agent, sender, body)
    if agent
      send_message_to_agent(agent, sender, body)
    else
      send_not_assigned_message(sender)
    end
  end

  def send_message_to_client(sender, body)
    return unless manager == sender
    # the sender is the agent of the user
    message_body = translate_message(body, locale)
    User.send_message(phone_number, twilio_number, message_body)
    Message.create(recipient: id, sender: sender.id, message: message_body)
    Analytics.track(user_id: sender.id, event: 'Agent Message', properties: { user: sender.name, recipient: name })
  end

  def send_no_relationship_message(sender)
    message_body = translate_message('No relationship found.', sender.locale)
    User.send_message(sender.phone_number,  ENV['TWILIO_NUMBER'], message_body)
    Message.create(recipient: sender.id, sender: sender.id, message: message_body)
    Analytics.track(user_id: sender.id, event: 'No Relationship Message', properties: { user: sender.name })
  end

  def send_no_active_subscription(sender)
    message_body = translate_message('This account does not have an active subscription. Type ENROLL to reenroll in Asha.', sender.locale)
    User.send_message(sender.phone_number,  ENV['TWILIO_NUMBER'], message_body)
    Message.create(recipient: sender.id, sender: User.admin.first.id, message: message_body)
    Analytics.track(user_id: sender.id, event: 'Inactive Subscription Message', properties: { user: sender.name })
  end

  def send_no_user(from)
    # Could not identify sender
    User.send_message(from, ENV['TWILIO_NUMBER'], 'Sorry, we do not recognize that phone number.')
    Message.create(recipient: '000', sender: '000', message: 'Sorry, we do not recognize that phone number.')
    Analytics.track(anonymous_id: rand(0..110_000), event: 'Unrecognized Number Message', properties: { user: 'None' })
  end

  def charge_the_user(amount, description)
    # :nocov:
    charge = Stripe::Charge.create(amount: (amount.to_f * 100).to_i, currency: 'usd', customer: subscription.stripe_id, description: description) unless Rails.env.test?
    Charge.create(user: self, token: Rails.env.test? ? '783293' : charge['id'], amount: amount, description: description)
    send_charge_success_message(amount)
  rescue Stripe::CardError
    send_charge_fail_message(amount)
    # :nocov:
  end

  def send_charge_success_message(amount)
    message_body = translate_message('Charge succeeded.', locale)
    User.send_message(phone_number,  twilio_number, message_body)
    Message.create(recipient: id, sender: User.admin.first.id, message: message_body)
    Analytics.track(user_id: id, event: 'Purchase Charge', properties: { amount: amount })
  end

  # :nocov:
  def send_charge_fail_message(amount)
    message_body = translate_message("Charge failed. Please update your payment information at #{ENV['DOMAIN_NAME']}", locale)
    User.send_message(phone_number,  twilio_number, message_body)
    Message.create(recipient: id, sender: User.admin.first.id, message: message_body)
    Analytics.track(user_id: id, event: 'Failed Purchase Charge', properties: { amount: amount })
  end
  # :nocov:

  def check_for_charge_authorization(body)
    return false unless (body.include?('CHARGING') && body.include?('FOR')) && body.include?('$')
    amount = body[/\$([0-9]+[\.]*[0-9]*)/].gsub('$', '')
    description = body[/(?<=FOR ).*/]
    return if amount == 0
    charge_the_user(amount, description)
  end

  def check_for_cancel_subscription(body)
    return false unless body.include?('CANCEL SUBSCRIPTION')
    if subscription && subscription.plan
      subscription.cancel
      send_subscription_cancel_message
    else
      send_subscription_cancel_fail_message
    end
  end

  def send_subscription_cancel_message
    message_body = EasyTranslate.translate('Your subscription has been cancelled. Type ENROLL to reenroll in Asha.', to: locale)
    User.send_message(phone_number,  twilio_number, message_body)
    Message.create(recipient: id, sender: User.admin.first.id, message: message_body)
    Analytics.track(user_id: id, event: 'Subscription Cancel', properties: {})
  end

  def send_subscription_cancel_fail_message
    message_body = EasyTranslate.translate('Subscription could not be found. Type ENROLL to reenroll in Asha.', to: locale)
    User.send_message(phone_number,  twilio_number, message_body)
    Message.create(recipient: id, sender: User.admin.first.id, message: message_body)
  end

  def check_for_reenrollment(body)
    return false unless body.include?('ENROLL')
    # :nocov:
    subscription.update_attributes(plan: Plan.first) unless Rails.env.test?
    # :nocov:
    message_body = EasyTranslate.translate("Congratulations. You are now enrolled to Asha at $#{Plan.first.price} per month.", to: locale)
    User.send_message(phone_number,  twilio_number, message_body)
    Message.create(recipient: id, sender: User.admin.first.id, message: message_body)
    Analytics.track(user_id: id, event: 'Subscription Reenroll', properties: {})
  end

  def parse_response(params)
    from = params[:From]
    body = params[:Body]
    sender = User.find_by(phone_number: from)
    if sender
      special_request = parse_special_requests(sender, body)
      active_subscription = subscription && subscription.plan
      return if special_request
      if active_subscription
        case sender
        when self
          send_message_if_has_agent(manager, sender, body)
        when manager
          send_message_to_client(sender, body)
        else
          send_no_relationship_message(sender)
        end
      else
        send_no_active_subscription(sender)
      end
    else
      send_no_user(from)
    end
  end

  def process_response(params)
    if params[:From] && params[:Body]
      parse_response(params)
      return { status: 'ok', message: 'Success!', html: '' }
    else
      return { status:  'ok', message: 'Not a valid text', html: '' }
    end
  end

  def parse_special_requests(sender, body)
    if [self, manager].include?(sender)
      a = [check_for_charge_authorization(body), check_for_cancel_subscription(body), check_for_reenrollment(body)]
      return a.include?(true)
    else
      return false
    end
  end
end

# Super class for a user
class Account < ActiveRecord::Base
  belongs_to :user
  belongs_to :client, class_name: 'User'
  validates_presence_of :user_id, :client_id
end

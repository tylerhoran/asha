# Blog Posts
class Post < ActiveRecord::Base
  extend FriendlyId
  translates :name, :body
  validates_presence_of :name, :body
  friendly_id :name, use: :slugged
  has_attached_file :image, styles: { medium: '304x192>', large: '671x415>', thumb: '55x55>' }, default_url: '/images/blog/:style.png'
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
  scope :recent, -> { order('created_at DESC') }
end

# Subscription plans for users
class Plan < ActiveRecord::Base
  include Koudoku::Plan
  belongs_to :user
  belongs_to :coupon
  has_many :subscriptions
  validates_presence_of :name, :stripe_id, :price, :interval
end

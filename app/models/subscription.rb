# The subscription record for each active users
class Subscription < ActiveRecord::Base
  include Koudoku::Subscription

  belongs_to :user
  belongs_to :coupon

  after_create :create_twilio_gateway
  after_create :assign_agent_to_user
  delegate :name, to: :user, prefix: true

  # :nocov:
  def cancel
    return if Rails.env.test?
    update_attributes(plan: nil)
    Subscription.destroy_all
  end
  # :nocov:

  def available_numbers(area_code)
    client.account.available_phone_numbers.get(user.territory).local.list(area_code: area_code)
  end

  def grab_twilio_numbers(client)
    response = client.account.incoming_phone_numbers.create(phone_number: available_numbers[0].phone_number, sms_url: CreateUrlService.new.sms, voice_url: CreateUrlService.new.voice)
    user.update_attributes(twilio_number: response.phone_number, twilio_pathway: response.sms_url)
    Analytics.track(user_id: user.id, event: 'Subscription Created', properties: { user: user.name, plan: plan.name })
  end
  # :nocov:

  def create_twilio_number(client)
    if Rails.env.test?
      user.set_test_numbers
    else
      # :nocov:
      grab_twilio_numbers(client)
      # :nocov:
    end
  end

  def create_twilio_gateway
    client = CreateUrlService.new.twilio_client
    GlobalPhone.parse(user.phone_number).territory.name
    create_twilio_number(client)
  end

  def assign_to_first_agent(available)
    available.first.clients << user
    User.send_message(user.phone_number, available.first.phone_number, "Hello, and welcome to Asha. I'm #{user.manager_name}. \
                            I'll be helping you all your requests. Ask for some examples by \
                            texting back 'EXAMPLES'. THanks for joining Asha!")
  end

  def send_over_limit_notices
    AdminMailer.over_limit.deliver_now
    User.send_message(user.phone_number, ENV['TWILIO_NUMBER'], "Hello. I'm Asha. Thanks for subscribing! \
                            We're working on assigning you an agent at the moment. \
                            We'll text you when that's all sorted.")
  end

  def available_agents
    User.agent.where(territory: user.territory).select { |avail| avail.clients.size < ENV['AGENT_CLIENT_LIMIT'].to_i }
  end

  def assign_agent_to_user
    return unless user.twilio_number && user.twilio_pathway
    available_agents.count > 0 ? assign_to_first_agent(available_agents) : send_over_limit_notices
  end

  def self.parse_json_response(data_json, subscription)
    return unless subscription
    case data_json['type']
    when 'invoice.payment_succeeded'
      amount = data_json['data']['object']['total'].to_f / 100.0
      subscription.payment_succeeded(amount)
    when 'charge.failed'
      subscription.charge_failed
    when 'charge.dispute.created'
      subscription.charge_disputed
    end
  end
end

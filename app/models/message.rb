# Saved records of each sent message
class Message < ActiveRecord::Base
  validates_presence_of :sender, :recipient, :message
end

# Records from the contact form
class Contact < ActiveRecord::Base
  validates_presence_of :name, :email, :message
  after_create :send_contact_message
  after_create :send_tracking

  def send_contact_message
    AdminMailer.new_contact_message(self).deliver_now
  end

  def send_tracking
    Analytics.track(anonymous_id: rand(0..110_000),
                    event: 'Contact Created',
                    properties: {
                      contact: name
                    })
  end
end

# Coupons for subscriptions
class Coupon < ActiveRecord::Base
  has_many :subscriptions
  validates_presence_of :code
end

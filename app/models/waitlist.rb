# Main model for storing waitlist users
class Waitlist < ActiveRecord::Base
  validates_presence_of :email, :name
  after_create :notify_users

  def notify_users
    AdminMailer.new_waitlist(self).deliver_now
    TrackingService.new.call(nil, User.new, 'Waiting list join')
  end
end

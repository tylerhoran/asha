# Twilio method for constructing assets and urls
class CreateUrlService
  def voice
    'https://' + ENV['DOMAIN_NAME'] + '/voice_response'
  end

  def sms
    'https://' + ENV['DOMAIN_NAME'] + '/users/' + user.id.to_s
  end

  def twilio_client
    Twilio::REST::Client.new(ENV['TWILIO_ACCOUNT_SID'], ENV['TWILIO_AUTH_TOKEN'])
  end
end

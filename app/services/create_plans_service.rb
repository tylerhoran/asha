# Seed method for creating plans
class CreatePlansService
  def call
    Stripe::Plan.all.each(&:delete)
    load('db/plans.rb')
    Plan.all.each do |plan|
      Stripe::Plan.create(name: plan.name, amount: (plan.price * 100).to_i, interval: plan.interval, currency: 'usd', id: plan.stripe_id)
    end
  end
end

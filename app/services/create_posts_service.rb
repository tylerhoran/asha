# Seed method for creating posts
class CreatePostsService
  def call
    load('db/posts.rb')
  end
end

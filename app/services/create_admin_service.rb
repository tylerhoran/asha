# Seed method for creating an admin user
class CreateAdminService
  def call
    password = ENV['ADMIN_PASSWORD']
    User.find_or_create_by!(email: ENV['ADMIN_EMAIL']) do |user|
      user.password = password
      user.password_confirmation = password
      user.admin!
    end
  end
end

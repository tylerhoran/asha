# Method for tracking activity via Segment.io
class TrackingService
  def call(user, property, event)
    hash = { property.class.name.to_sym => property.name }
    user ? Analytics.track(user_id: user.id, event: event, properties: hash) : Analytics.track(anonymous_id: rand(0..110_000), event: event, properties: hash)
  end
end

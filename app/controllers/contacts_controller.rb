# Controller of the contact form
class ContactsController < ApplicationController
  before_action :set_contact, only: [:show, :edit, :update, :destroy]
  respond_to :html, :json

  def new
    @contact = Contact.new
  end

  def edit
  end

  def show
  end

  def create
    @contact = Contact.new(contact_params)
    flash[:notice] = "Thanks for the message. We'll be in touch soon." if @contact.save
    respond_with(@contact)
  end

  private

  def contact_params
    params.require(:contact).permit(:name, :email, :message)
  end
end

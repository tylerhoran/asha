# The main controller for the static pages
class VisitorsController < ApplicationController
  def privacy
  end

  def features
    TrackingService.new.call(current_user, User.new, 'Features view')
  end

  def terms
  end

  def index
    @user_count = User.count.to_s.scan(/./).map(&:to_i)
    TrackingService.new.call(current_user, User.new, 'Landing view')
  end

  def ping
  end
end

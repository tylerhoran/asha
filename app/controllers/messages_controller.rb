# The controller for voice responses
class MessagesController < ApplicationController
  layout false
  protect_from_forgery except: :voice_response

  def voice_response
    TrackingService.new.call(current_user, User.new, 'Called user')
  end
end

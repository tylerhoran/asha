module Koudoku
  # The callbacks controller for subscriptions
  class WebhooksController < ApplicationController
    protect_from_forgery except: [:create]

    def create
      data_json = JSON.parse(request.body.read)
      subscription = ::Subscription.find_by_stripe_id(data_json['data']['object']['customer'])
      ::Subscription.parse_json_response(data_json, subscription)
      render nothing: true
    end
  end
end

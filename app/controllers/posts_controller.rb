# The blog controller
class PostsController < ApplicationController
  before_action :set_post, only: [:show]
  respond_to :html

  def index
    TrackingService.new.call(current_user, User.new, 'Blog view')
    @posts = Post.recent.page(params[:page])
    respond_with(@posts)
  end

  def show
    TrackingService.new.call(current_user, @post, 'Blog post view')
    @posts = Post.recent.page(params[:page])
    respond_with(@post)
  end

  private

  def set_post
    @post = Post.friendly.find(params[:id])
  end
end

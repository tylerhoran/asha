# Controller showing charges made to the user
class ChargesController < ApplicationController
  before_action :authenticate_user!

  def index
    @charges = current_user.charges
  end
end

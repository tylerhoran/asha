# Main controller for getting on the waitlist
class WaitlistsController < ApplicationController
  respond_to :html

  def new
    @waitlist = Waitlist.new
    respond_with(@waitlist)
    TrackingService.new.call(current_user, User.new, 'Waiting list view')
  end

  def create
    @waitlist = Waitlist.new(waitlist_params)
    if @waitlist.save
      flash[:notice] = "Thanks for joining the waitlist. We'll be reaching out soon"
      redirect_to root_path
    else
      render :new
    end
  end

  private

  def waitlist_params
    params.require(:waitlist).permit(:name, :email, :locale)
  end
end

module Users
  # Manages the registrations params for the application
  class RegistrationsController < Devise::RegistrationsController
    before_filter :configure_permitted_parameters

    protected

    def configure_permitted_parameters
      devise_parameter_sanitizer.for(:account_update) do |update|
        update.permit(:name, :phone_number,
                      :email, :password, :password_confirmation, :current_password, :locale)
      end
    end
  end
end

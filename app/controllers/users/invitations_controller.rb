module Users
  # Managing invitations for the appliction
  class InvitationsController < Devise::InvitationsController
    before_action :check_admin, only: [:new]

    private

    def check_admin
      redirect_to root_path, flash: { error: 'You must be admin.' } unless current_user && current_user.admin?
    end
  end
end

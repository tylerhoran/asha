# This is the main application controller setting tracking and devise params
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :devise_configure_permitted_parameters, if: :devise_controller?

  protected

  def devise_configure_permitted_parameters
    devise_parameter_sanitizer.for(:accept_invitation) << [:name, :phone_number, :locale]
    devise_parameter_sanitizer.for(:account_update) << [:name, :phone_number, :email, :password, :password_confirmation, :current_password, :locale]
  end
end

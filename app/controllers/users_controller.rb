# The main controller for processing text responses
class UsersController < ApplicationController
  before_filter :authenticate_user!, except: :show
  before_filter :admin_only, except: :show
  protect_from_forgery except: :show
  respond_to :json

  def show
    @user = User.find_by(id: params['id'])
    if @user
      @msg = @user.process_response(params)
    else
      @msg = { status: 'ok', message: 'Not a valid user', html: '' }
    end
    render json: @msg
  end
end

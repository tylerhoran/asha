require 'email_spec'

feature 'Waitlist' do
  scenario 'joining from the homepage' do
    visit root_path
    click_link 'Join'
    fill_in 'Name', with: Faker::Internet.name
    fill_in 'Email', with: Faker::Internet.email
    click_button 'Join'
    expect(Waitlist.count).to eq(1)
    expect(page).to have_content I18n.t('landing.global_sms_assistant')
    expect(ActionMailer::Base.deliveries.last.body).to have_content 'There has been a new subscriber to the waitlist'
  end

  scenario 'joining without entering email' do
    visit root_path
    click_link 'Join'
    fill_in 'Name', with: Faker::Internet.name
    click_button 'Join'
    expect(page).to have_content "Email can't be blank"
  end
end

feature 'Inviting' do
  before :each do
    @user = FactoryGirl.create(:user)
    @user.admin!
    signin(@user.email, 'please123')
  end

  scenario 'cannot be accessed by non admins' do
    @user.user!
    visit new_user_invitation_path
    expect(page).to have_content 'You must be admin'
  end

  scenario 'users individually from the admin' do
    visit new_user_invitation_path
    fill_in 'Email', with: Faker::Internet.email
    click_button 'Invite'
    expect(ActionMailer::Base.deliveries.last.body).to have_content 'Someone has invited you to'
  end

  scenario 'users can accept invitation' do
    phone_number = Faker::PhoneNumber.cell_phone
    name = Faker::Name.name
    @plans = FactoryGirl.create_list(:plan, 3)
    @email = Faker::Internet.email
    @password = Faker::Internet.password
    visit new_user_invitation_path
    fill_in 'Email', with: @email
    click_button 'Invite'
    open_email(@email)
    current_email.click_link 'Accept invitation'
    fill_in 'user_name', with: name
    fill_in 'user_phone_number', with: phone_number
    fill_in 'user_password', with: @password
    fill_in 'user_password_confirmation', with: @password
    click_button 'Continue'
    click_link 'Edit Account'
    expect(find_field('user_phone_number').value).to eq(GlobalPhone.parse(phone_number).international_string)
    expect(find_field('user_name').value).to eq(name)
  end
end

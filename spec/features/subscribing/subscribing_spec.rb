feature 'Subscribing' do
  before :each do
    if Stripe::Plan.all.count == 3
      Stripe::Plan.all.each_with_index do |plan, index|
        @plan = FactoryGirl.create(:plan,
                                   name: plan.name,
                                   price: plan.amount / 100,
                                   interval: plan.interval,
                                   stripe_id: plan.id,
                                   features: ['Private experiments community',
                                              '10 experiments',
                                              '100 participants'].join('\n\n'),
                                   display_order: index + 1)
      end
    else
      CreatePlansService.new.call
    end
  end

  scenario 'shows plans on the pricing page' do
    user = FactoryGirl.create(:user)
    signin(user.email, user.password)
    visit pricing_path
    expect(page).to have_content @plan.name
  end

  scenario 'from the home page not logged in adds twilio number and gateway and agent welcome', js: true do
    user = FactoryGirl.create(:user)
    agent = FactoryGirl.create(:user)
    agent.agent!
    signin(user.email, user.password)
    visit "/base/subscriptions/new?plan=#{@plan.id}"
    fill_in 'card-number', with: '4242424242424242'
    fill_in 'card-expiry-month', with: '12'
    fill_in 'card-expiry-year', with: '2019'
    fill_in 'card-cvc', with: '289'
    click_button 'Add Payment Information'
    sleep(2.seconds)
    expect(page).to have_content "You've been successfully upgraded."
    expect(User.find_by(email: user.email).twilio_number).to_not eq(nil)
    expect(User.find_by(email: user.email).twilio_pathway).to_not eq(nil)
  end

  scenario 'changing plans upgrades user', js: true do
    user = FactoryGirl.create(:user)
    signin(user.email, user.password)
    @plan_2 = Plan.last
    visit "/base/subscriptions/new?plan=#{@plan_2.id}"
    fill_in 'card-number', with: '4242424242424242'
    fill_in 'card-expiry-month', with: '12'
    fill_in 'card-expiry-year', with: '2019'
    fill_in 'card-cvc', with: '289'
    click_button 'Add Payment Information'
    sleep(2.seconds)
    visit pricing_path
    Subscription.last.update_attributes(plan: @plan_2)
    expect(User.last.subscription.plan).to eq(@plan_2)
  end

  scenario 'signing in after subscription goes to user edit', js: true do
    user = FactoryGirl.create(:user)
    signin(user.email, user.password)
    visit "/base/subscriptions/new?plan=#{@plan.id}"
    fill_in 'card-number', with: '4242424242424242'
    fill_in 'card-expiry-month', with: '12'
    fill_in 'card-expiry-year', with: '2019'
    fill_in 'card-cvc', with: '289'
    click_button 'Add Payment Information'
    sleep(2.seconds)
    click_link 'Sign Out'
    signin(user.email, user.password)
    expect(page).to have_content('Signed in successfully')
  end

  scenario 'visiting pricing path redirects after subscribing', js: true do
    user = FactoryGirl.create(:user)
    signin(user.email, user.password)
    visit "/base/subscriptions/new?plan=#{@plan.id}"
    fill_in 'card-number', with: '4242424242424242'
    fill_in 'card-expiry-month', with: '12'
    fill_in 'card-expiry-year', with: '2019'
    fill_in 'card-cvc', with: '289'
    click_button 'Add Payment Information'
    sleep(2.seconds)
    visit pricing_path
    expect(page).to have_content 'You can also cancel your subscription'
  end

  scenario 'canceling a subscription', js: true do
    user = FactoryGirl.create(:user)
    signin(user.email, user.password)
    visit "/base/subscriptions/new?plan=#{@plan.id}"
    fill_in 'card-number', with: '4242424242424242'
    fill_in 'card-expiry-month', with: '12'
    fill_in 'card-expiry-year', with: '2019'
    fill_in 'card-cvc', with: '289'
    click_button 'Add Payment Information'
    sleep(2.seconds)
    visit pricing_path
    click_link 'cancel your subscription'
    expect(page).to have_content "You've successfully cancelled your subscription"
  end

  scenario 'visiting another users subscription account fails' do
    user = FactoryGirl.create(:user)
    signin(user.email, user.password)
    visit '/base/users/789/subscriptions'
    expect(page).to have_content 'Unauthorized'
  end
end

feature 'Viewing charges' do
  before :each do
    @user = FactoryGirl.create(:user)
    @plans = FactoryGirl.create_list(:plan, 3)
    @charge = FactoryGirl.create(:charge, user: @user)
    signin(@user.email, 'please123')
  end

  scenario 'from the index page' do
    visit charges_path
    expect(page).to have_content @charge.amount
  end
end

include Warden::Test::Helpers
Warden.test_mode!

feature 'User delete', :devise, :js do
  after(:each) do
    Warden.test_reset!
  end

  scenario 'user can delete own account', js: true do
    user = FactoryGirl.create(:user)
    login_as(user, scope: :user)
    visit edit_user_registration_path(user)
    click_link 'Cancel my account'
    expect(page).to have_content I18n.t 'devise.registrations.destroyed'
  end
end

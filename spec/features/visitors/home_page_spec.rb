feature 'Home page' do
  scenario 'visit the home page' do
    visit root_path
    expect(page).to have_content I18n.t('landing.global_sms_assistant')
  end

  scenario 'visit the privacy page' do
    visit root_path
    expect(page).to have_content 'Privacy'
  end

  scenario 'visit the terms page' do
    visit root_path
    expect(page).to have_content 'Terms'
  end

  scenario 'visit the contact page' do
    visit root_path
    expect(page).to have_content 'Contact'
  end

  scenario 'visit the features page' do
    visit features_path
    expect(page).to have_content 'Asha Applications'
  end

  scenario 'visit the ping page' do
    visit ping_path
    expect(page).to have_content 'test'
  end
end

feature 'Contacting us' do
  scenario 'from the contact page' do
    visit new_contact_path
    fill_in 'contact_name', with: Faker::Internet.name
    fill_in 'contact_email', with: Faker::Internet.email
    fill_in 'contact_phone', with: Faker::PhoneNumber.phone_number
    fill_in 'contact_message', with: Faker::Lorem.paragraph
    click_button 'Send your message'
    expect(page).to have_content "Thanks for the message. We'll be in touch soon."
  end

  scenario 'with missing elements' do
    visit new_contact_path
    fill_in 'contact_name', with: Faker::Internet.name
    click_button 'Send your message'
    expect(page).to have_content "Email can't be blank"
  end
end

feature 'Navigation links', :devise do
  scenario 'view navigation links' do
    visit root_path
    expect(page).to have_content I18n.t('landing.global_sms_assistant')
    expect(page).to have_content 'Sign In'
    expect(page).to have_content 'Join'
  end
end

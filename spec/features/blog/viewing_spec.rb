feature 'Blog posts' do
  before :each do
    @posts = FactoryGirl.create_list(:post, 3)
    @post = @posts.first
  end

  scenario 'show on the index page' do
    visit posts_path
    expect(page).to have_content @post.name
  end

  scenario 'visit the show page' do
    visit post_path(@post)
    expect(page).to have_content @post.name
  end
end

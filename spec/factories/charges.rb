FactoryGirl.define do
  factory :charge do
    association :user
    token 'MyString'
    amount 100.00
    description 'MyString'
  end
end

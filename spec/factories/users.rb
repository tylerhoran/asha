FactoryGirl.define do
  factory :user do
    confirmed_at Time.now
    name 'Test User'
    email { Faker::Internet.email }
    password 'please123'
    phone_number { "+1555404444#{rand(0..9)}" }
    locale 'en'
    trait :admin do
      role 'admin'
    end
  end
end

require 'codeclimate-test-reporter'
require 'capybara/email/rspec'
require 'simplecov'
Capybara.javascript_driver = :webkit
RSpec.configure do |c|
  c.treat_symbols_as_metadata_keys_with_true_values = true
  SimpleCov.start 'rails'
  SimpleCov.minimum_coverage 90
  CodeClimate::TestReporter.start if ENV['CODECLIMATE_REPO_TOKEN']
  I18n.default_locale = :en
  I18n.locale = :en
  Dir[Rails.root.join('app/models/**/*.rb')].each { |f| require f }
  Dir[Rails.root.join('app/controllers/**/*.rb')].each { |f| require f }
  Dir[Rails.root.join('app/lib/**/*.rb')].each { |f| require f }
end

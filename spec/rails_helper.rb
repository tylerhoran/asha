require 'spec_helper'
require File.expand_path('../../config/environment', __FILE__)
require 'rspec/rails'

Dir[Rails.root.join('spec/support/**/*.rb')].each { |f| require f }
ActiveRecord::Migration.maintain_test_schema!

RSpec.configure do |config|
  config.fixture_path = '#{::Rails.root}/spec/fixtures'
  config.use_transactional_fixtures = false
  config.infer_spec_type_from_file_location!
  config.before(:each, js: true) do
    page.driver.allow_url 'https://api.stripe.com/v1/tokens'
    page.driver.allow_url 'https://js.stripe.com/v2/'
    page.driver.allow_url 'https://js.stripe.com/v1/'
  end
end

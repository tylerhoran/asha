require 'spec_helper'
require 'sms_spec'

describe UsersController do
  include SmsSpec::Helpers
  include SmsSpec::Matchers
  before :each do
    @params = { ToCountry: 'US',
                ToState: 'NY',
                SmsMessageSid: 'SM3c6488e4eed6697a67b7b3aa51717895',
                NumMedia: '0',
                ToCity: 'NEW YORK',
                FromZip: '94014',
                SmsSid: 'SM3c6488e4eed6697a67b7b3aa51717895',
                FromState: 'CA',
                SmsStatus: 'received',
                FromCity: 'SAN FRANCISCO',
                Body: 'C',
                FromCountry: 'US',
                To: '+15005550006',
                ToZip: '10099',
                MessageSid: 'SM3c6488e4eed6697a67b7b3aa51717895',
                AccountSid: 'AC4bc44c7997d7177010f8dcdf67323d3a',
                From: '+15005550006',
                ApiVersion: '2010-04-01',
                id: 2322 }
  end
  context 'without a registered user' do
    it "should reply that the user doesn't exist" do
      request.accept = 'application/json'
      post :show, @params
      expect(response.body).to have_content 'Not a valid user'
    end
  end

  context 'without an enrolled experiment' do
    it "should respond that user doesn't have any enrollments" do
      @user = FactoryGirl.create(:user)
      request.accept = 'application/json'
      @params[:id] = @user.id
      post :show, @params
    end
  end

  context 'without all the required params' do
    it 'replies with invalid text' do
      request.accept = 'application/json'
      @user = FactoryGirl.create(:user)
      params = { id: @user.id }
      post :show, params
      expect(response.body).to have_content 'Not a valid text'
    end
  end

  context 'sending to a subscribed user' do
    before :each do
      @user = FactoryGirl.create(:user)
      @user.update_attributes(twilio_number: '+15005550006', twilio_pathway: 'https://' + ENV['DOMAIN_NAME'] + '/users/' + @user.id.to_s)
      @agent = FactoryGirl.create(:user)
      @agent.agent!
      @admin = FactoryGirl.create(:user)
      @admin.admin!
      @plans = FactoryGirl.create_list(:plan, 3)
      @params = { id: @user.id, From: @agent.phone_number, to: '+15005550006' }
    end

    it 'sending a charge for an item' do
      @subscription = Subscription.new(user: @user, plan: @plans.first, stripe_id: 'ffjklsdjfskdlfsd', last_four: '4242', card_type: 'undefined', current_price: @plans.first.price).sneaky_save
      Subscription.last.assign_agent_to_user
      request.accept = 'application/json'
      @params[:Body] = 'CHARGING $100.00 FOR something silly'
      post :show, @params
      expect(Message.last.message).to have_content 'Charge succeeded.'
    end

    it 'sending a subscription cancelation' do
      @subscription = Subscription.new(user: @user, plan: @plans.first, stripe_id: 'ffjklsdjfskdlfsd', last_four: '4242', card_type: 'undefined', current_price: @plans.first.price).sneaky_save
      Subscription.last.assign_agent_to_user
      request.accept = 'application/json'
      @params[:Body] = 'CANCEL SUBSCRIPTION'
      post :show, @params
      expect(Message.last.message).to have_content 'Your subscription has been cancelled'
    end

    it 'sending a re-enrollment request' do
      @subscription = Subscription.new(user: @user, plan: @plans.first, stripe_id: 'ffjklsdjfskdlfsd', last_four: '4242', card_type: 'undefined', current_price: @plans.first.price).sneaky_save
      Subscription.last.assign_agent_to_user
      request.accept = 'application/json'
      @params[:Body] = 'CANCEL SUBSCRIPTION'
      post :show, @params
      expect(Message.last.message).to have_content 'Your subscription has been cancelled'
      @params[:Body] = 'ENROLL'
      post :show, @params
      expect(Message.last.message).to have_content 'Congratulations. You are now enrolled to Asha'
    end

    it 'sends a reply with no subscription without enrollment' do
      request.accept = 'application/json'
      @params[:Body] = 'CANCEL SUBSCRIPTION'
      post :show, @params
      @params[:Body] = 'Hey, can you do this for me?'
      @params[:From] = @user.phone_number
      post :show, @params
      expect(Message.last.message).to have_content 'This account does not have an active subscription'
    end

    it 'sends an already canceled message on second cancel' do
      request.accept = 'application/json'
      @params[:Body] = 'CANCEL SUBSCRIPTION'
      post :show, @params
      @params[:Body] = 'CANCEL SUBSCRIPTION'
      @params[:From] = @user.phone_number
      post :show, @params
      expect(Message.last.message).to have_content 'This account does not have an active subscription. Type ENROLL to reenroll in Asha.'
    end

    it 'sends a message that the sender is not registered if random number' do
      @subscription = Subscription.new(user: @user, plan: @plans.first, stripe_id: 'ffjklsdjfskdlfsd', last_four: '4242', card_type: 'undefined', current_price: @plans.first.price).sneaky_save
      Subscription.last.assign_agent_to_user
      request.accept = 'application/json'
      @params[:From] = '+13329839203'
      @params[:Body] = 'fjdskfds'
      post :show, @params
      expect(Message.last.message).to have_content('Sorry, we do not recognize that phone number.')
    end

    it 'send a non-recognizable relationship if the wrong user' do
      @subscription = Subscription.new(user: @user, plan: @plans.first, stripe_id: 'ffjklsdjfskdlfsd', last_four: '4242', card_type: 'undefined', current_price: @plans.first.price).sneaky_save
      Subscription.last.assign_agent_to_user
      @user2 = FactoryGirl.create(:user)
      @subscription = Subscription.new(user: @user2,
                                       plan: @plans.first,
                                       stripe_id: 'ffjklsdjfskdlfsd',
                                       last_four: '4242',
                                       card_type: 'undefined',
                                       current_price: @plans.first.price).sneaky_save
      request.accept = 'application/json'
      @params[:From] = @user2.phone_number
      @params[:Body] = 'fjdskfds'
      post :show, @params
      expect(Message.last.message).to have_content 'No relationship found.'
    end

    context 'as an agent' do
      it 'sends a message to the user if the authorized agent' do
        @subscription = Subscription.new(user: @user, plan: @plans.first, stripe_id: 'ffjklsdjfskdlfsd', last_four: '4242', card_type: 'undefined', current_price: @plans.first.price).sneaky_save
        Subscription.last.assign_agent_to_user
        request.accept = 'application/json'
        @params[:From] = @agent.phone_number
        @params[:Body] = 'Hey, I have response to your request.'
        post :show, @params
        expect(Message.last.message).to have_content 'Hey, I have response to your request.'
      end

      it 'should translate to the language of the user' do
        @user.update_attributes(locale: 'es')
        @subscription = Subscription.new(user: @user, plan: @plans.first, stripe_id: 'ffjklsdjfskdlfsd', last_four: '4242', card_type: 'undefined', current_price: @plans.first.price).sneaky_save
        Subscription.last.assign_agent_to_user
        request.accept = 'application/json'
        @params[:From] = @agent.phone_number
        @params[:Body] = 'Hey, I have response to your request.'
        post :show, @params
        expect(Message.last.message).to have_content(EasyTranslate.translate('Hey, I have response to your request.', to: 'es'))
      end
    end

    context 'as a subscriber' do
      it 'sends a message to the agent if one has been assigned' do
        @subscription = Subscription.new(user: @user, plan: @plans.first, stripe_id: 'ffjklsdjfskdlfsd', last_four: '4242', card_type: 'undefined', current_price: @plans.first.price).sneaky_save
        Subscription.last.assign_agent_to_user
        request.accept = 'application/json'
        @params[:From] = @user.phone_number
        @params[:Body] = 'Hey, I have a request.'
        post :show, @params
        expect(Message.last.message).to have_content 'Hey, I have a request.'
      end

      it 'should translate to the language of the agent' do
        @agent.update_attributes(locale: 'es')
        @subscription = Subscription.new(user: @user, plan: @plans.first, stripe_id: 'ffjklsdjfskdlfsd', last_four: '4242', card_type: 'undefined', current_price: @plans.first.price).sneaky_save
        Subscription.last.assign_agent_to_user
        request.accept = 'application/json'
        @params[:From] = @user.phone_number
        @params[:Body] = 'Hey, I have a request.'
        post :show, @params
        expect(Message.last.message).to have_content(EasyTranslate.translate('Hey, I have a request.', to: 'es'))
      end

      it 'sends a reply if no agents has been assigned yet' do
        @subscription = Subscription.new(user: @user, plan: @plans.first, stripe_id: 'ffjklsdjfskdlfsd', last_four: '4242', card_type: 'undefined', current_price: @plans.first.price).sneaky_save
        Subscription.last.assign_agent_to_user
        @agent.clients.destroy_all
        request.accept = 'application/json'
        @params[:From] = @user.phone_number
        @params[:Body] = 'Hey, I have a request.'
        post :show, @params
        expect(Message.last.message).to have_content 'You have not been assigned an agent yet.'
      end
    end
  end
end

require 'spec_helper'

describe MessagesController do
  context 'without a registered user' do
    it "should reply that the user doesn't exist" do
      post :voice_response
      expect(response.status).to eq(200)
    end
  end
end

require 'rails_helper'

describe 'Coupon' do
  before :each do
    @coupon = FactoryGirl.build(:coupon)
  end

  it 'should be valid with all attributes' do
    expect(@coupon).to be_valid
  end

  it 'should not be valid without a code' do
    @coupon.code = nil
    expect(@coupon).to_not be_valid
  end
end

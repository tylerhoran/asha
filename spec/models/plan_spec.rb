require 'rails_helper'

describe 'Plan' do
  before :each do
    @plan = FactoryGirl.build(:plan)
  end

  it 'should be valid with all attributes' do
    expect(@plan).to be_valid
  end

  it 'should not be valid without a price' do
    @plan.price = nil
    expect(@plan).to_not be_valid
  end

  it 'should not be valid without a stripe id' do
    @plan.stripe_id = nil
    expect(@plan).to_not be_valid
  end

  it 'should not be valid without a name' do
    @plan.name = nil
    expect(@plan).to_not be_valid
  end

  it 'should not be valid without an interval' do
    @plan.interval = nil
    expect(@plan).to_not be_valid
  end
end

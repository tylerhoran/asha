require 'rails_helper'

describe 'Message' do
  before :each do
    @message = FactoryGirl.build(:message)
  end

  it 'should be valid with all attributes' do
    expect(@message).to be_valid
  end

  it 'should not be valid without a sender' do
    @message.sender = nil
    expect(@message).to_not be_valid
  end

  it 'should not be valid without a recipient' do
    @message.recipient = nil
    expect(@message).to_not be_valid
  end

  it 'should not be valid without a message' do
    @message.message = nil
    expect(@message).to_not be_valid
  end
end

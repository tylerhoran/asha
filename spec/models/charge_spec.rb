require 'rails_helper'

describe 'Charge' do
  before :each do
    @charge = FactoryGirl.build(:charge)
  end

  it 'should be valid with all attributes' do
    expect(@charge).to be_valid
  end

  it 'should not be valid without a user' do
    @charge.user = nil
    expect(@charge).to_not be_valid
  end

  it 'should not be valid without a token' do
    @charge.token = nil
    expect(@charge).to_not be_valid
  end

  it 'should not be valid without an amount' do
    @charge.amount = nil
    expect(@charge).to_not be_valid
  end

  it 'should not be valid without a description' do
    @charge.description = nil
    expect(@charge).to_not be_valid
  end
end

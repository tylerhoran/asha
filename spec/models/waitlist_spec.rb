require 'rails_helper'

describe 'Waitlist' do
  before :each do
    @waitlist = FactoryGirl.create(:waitlist)
  end

  it 'should be valid with all attributes' do
    expect(@waitlist).to be_valid
  end

  it 'should not be valid without an email' do
    @waitlist.email = nil
    expect(@waitlist).to_not be_valid
  end

  it 'should not be valid without a name' do
    @waitlist.name = nil
    expect(@waitlist).to_not be_valid
  end
end

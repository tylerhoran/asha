require 'rails_helper'

describe 'Contact' do
  before :each do
    @contact = FactoryGirl.build(:contact)
  end

  it 'should be valid with all attributes' do
    expect(@contact).to be_valid
  end

  it 'should not be valid without a name' do
    @contact.name = nil
    expect(@contact).to_not be_valid
  end

  it 'should not be valid without an email' do
    @contact.email = nil
    expect(@contact).to_not be_valid
  end

  it 'should not be valid without a message' do
    @contact.message = nil
    expect(@contact).to_not be_valid
  end
end

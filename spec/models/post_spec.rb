require 'rails_helper'

describe 'Post' do
  before :each do
    @post = FactoryGirl.build(:post)
  end

  it 'should be valid with all attributes' do
    expect(@post).to be_valid
  end

  it 'should not be valid without a title' do
    @post.name = nil
    expect(@post).to_not be_valid
  end

  it 'should not be valid without a body' do
    @post.body = nil
    expect(@post).to_not be_valid
  end
end

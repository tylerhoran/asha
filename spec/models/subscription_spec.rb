require 'rails_helper'

describe 'Subscription' do
  before :each do
    @user = FactoryGirl.create(:user)
    @subscription = FactoryGirl.build(:subscription, user: @user)
  end

  it 'should be valid with all attributes' do
    expect(@subscription).to be_valid
  end
end

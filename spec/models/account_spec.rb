require 'rails_helper'

describe 'Account' do
  before :each do
    @account = FactoryGirl.build(:account)
  end

  it 'should be valid with all attributes' do
    expect(@account).to be_valid
  end

  it 'should not be valid without a user' do
    @account.user_id = nil
    expect(@account).to_not be_valid
  end

  it 'should not be valid without a client' do
    @account.client_id = nil
    expect(@account).to_not be_valid
  end
end
